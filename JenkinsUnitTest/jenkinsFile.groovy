pipeline {
    agent any
    environment {
        PORT = '44387'
    }
    stages {
        stage('Descargar repositorios') {
            steps { 
                echo '----------------- Descargando repositorio del Proyecto -----------------'
                
                checkout([$class: 'GitSCM', branches: [[name: '*/main']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'Proyecto']], userRemoteConfigs: [[credentialsId: '013887a6-50d3-4097-b3db-2505efbfa310', url: 'https://gitlab.com/NatalioChiabo/jenkinstest.git']]])
                sleep 5
                
                echo '----------------- Descargando repositorio del Pruebas -----------------'
                
                checkout([$class: 'GitSCM', branches: [[name: '*/main']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'Pruebas']], userRemoteConfigs: [[credentialsId: '013887a6-50d3-4097-b3db-2505efbfa310', url: 'https://gitlab.com/NatalioChiabo/jenkinsunittest.git']]])
            }
        }
        stage('Compilando proyectos') {
            steps { 
                echo '----------------- Compilando Proyecto -----------------'
                
                bat "\"${tool name: 'Visual Studio 2019', type: 'msbuild'}msbuild.exe\" Proyecto\\JenkinsTest.sln /p:Configuration=Debug /p:Platform=\"Any CPU\" /p:ProductVersion=1.0.0.${env.BUILD_NUMBER}"
                
                echo '----------------- Compilando Pruebas -----------------'
                
                bat "\"${tool name: 'Visual Studio 2019', type: 'msbuild'}msbuild.exe\" Pruebas\\JenkinsUnitTest.sln /p:Configuration=Debug /p:Platform=\"Any CPU\" /p:ProductVersion=1.0.0.${env.BUILD_NUMBER}"
            }
        }
        
        stage('Inicializando Proyecto') {
            steps { 
                echo '----------------- Ejecutando Proyecto -----------------'
                bat "START \"webpage\" /b /d \"${ISSExpress}\" issexpress.exe /path:\"Proyecto\" /port:${PORT} /clr:v2.0"
                sleep 5
            }
        }

        stage('Ejecutando Pruebas'){
            steps{
                echo '----------------- Ejecutando Pruebas -----------------'

                bat 'Pruebas\\JenkinsUnitTest\\bin\\Debug\\net5.0\\JenkinsUnitTest.exe'
            }
        }
        
        stage('Cerrando el Proyecto'){
            steps{
                echo '----------------- Terminando la ejecucion del proyecto -----------------'

                bat "taskkill /IM iisexpress.exe"
            }
        }

        stage('Reportando resultados'){
            steps{
                echo '----------------- Enviando reporte -----------------'
                
                //emailext attachmentsPattern: 'Pruebas/JenkinsUnitTest/bin/Debug/net5.0/Capturas/*.jpg', body: '${FILE,path="Pruebas/JenkinsUnitTest/bin/Debug/net5.0/Email/Email_Output.html"}', replyTo: '$DEFAULT_REPLYTO', subject: '$DEFAULT_SUBJECT', to: '$DEFAULT_RECIPIENTS'
            }
        }
    }
}
